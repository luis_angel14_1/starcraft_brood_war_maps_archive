This StarCraft maps archive was downloaded from www.scmaps.net

Archive last updated: Nov. 25 2016

Total number of maps in archive: 19,708

All maps are unique based on file size and MD5 hash.

Have a bunch of old Starcraft / Brood War maps? We want them!

E-Mail us: scmaps.net@gmail.com
